package com.example.demo.notas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.notas.model.Curso;
import com.example.demo.notas.repository.CursoRepository;

@Service
public class CursoServiceImpl implements CursoService{

	@Autowired
	CursoRepository repository;
	
	@Override
	@Transactional(readOnly = true)
	public Iterable<Curso> findAll() {
		
		return repository.findAll();
	}
	
}
