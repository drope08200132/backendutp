package com.example.demo.notas.service;

import java.util.Optional;

import com.example.demo.notas.model.Alumno;

public interface AlumnoService {
	public Iterable<Alumno> findAll();
	public Optional<Alumno> findAlumnoById(Long id);
}
