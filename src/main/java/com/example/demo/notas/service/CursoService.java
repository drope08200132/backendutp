package com.example.demo.notas.service;

import com.example.demo.notas.model.Curso;

public interface CursoService {
	public Iterable<Curso> findAll();
}
