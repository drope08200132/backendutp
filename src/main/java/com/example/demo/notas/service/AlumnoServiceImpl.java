package com.example.demo.notas.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.notas.model.Alumno;
import com.example.demo.notas.repository.AlumnoRepository;

@Service
public class AlumnoServiceImpl  implements AlumnoService{
	@Autowired
	AlumnoRepository repository;
	
	@Override
	@Transactional(readOnly = true)
	public Iterable<Alumno> findAll() {
		
		return repository.findAll();
	}
	
	@Override
	@Transactional(readOnly = true)
	public Optional<Alumno> findAlumnoById(Long id) {
		
		return repository.findById(id);
	}
}
