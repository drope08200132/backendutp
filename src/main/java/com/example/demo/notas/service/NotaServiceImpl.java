package com.example.demo.notas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.notas.model.Nota;
import com.example.demo.notas.repository.NotaRepository;

@Service
public class NotaServiceImpl implements NotaService{
	@Autowired
	private NotaRepository repository;

	@Override
	@Transactional(readOnly = true)
	public Iterable<Nota> findAll() {
		
		return repository.findAll();
	}

	@Override
	@Transactional
	public Nota save(Nota entity) {
		
		return repository.save(entity);
	}

	@Override
	@Transactional(readOnly = true)
	public Iterable<Nota> ObtenerNotasPorIdAlumno(Long id) {
		return repository.ObtenerNotasPorIdAlumno(id);
		
	}

}
