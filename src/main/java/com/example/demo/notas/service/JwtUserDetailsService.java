package com.example.demo.notas.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.notas.model.Alumno;
import com.example.demo.notas.repository.AlumnoRepository;
@Service
public class JwtUserDetailsService implements UserDetailsService {
	
	@Autowired
	private AlumnoRepository repository;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Alumno user = repository.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("Alumno no encontrado con el username: " + username);
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				new ArrayList<>());
	}
	
	public Alumno save(Alumno user) {
		Alumno newUser = new Alumno();
		newUser.setUsername(user.getUsername());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		newUser.setNombre(user.getNombre());
		newUser.setApellido(user.getApellido());
		return repository.save(newUser);
	}

}
