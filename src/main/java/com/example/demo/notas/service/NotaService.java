package com.example.demo.notas.service;

import com.example.demo.notas.model.Nota;

public interface NotaService {
	public Iterable<Nota> findAll();
	public Nota save(Nota nota);
	public Iterable<Nota> ObtenerNotasPorIdAlumno(Long id);

}
