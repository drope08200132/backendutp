package com.example.demo.notas.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.notas.model.Nota;

@Repository
public interface NotaRepository  extends CrudRepository<Nota, Long>{

	@Query("select n from Nota n join fetch n.alumno a where a.id=?1")
	public Iterable<Nota> ObtenerNotasPorIdAlumno(Long id);
}
