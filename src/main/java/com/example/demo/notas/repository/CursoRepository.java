package com.example.demo.notas.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.notas.model.Curso;

@Repository
public interface CursoRepository extends CrudRepository<Curso, Long> {

}
