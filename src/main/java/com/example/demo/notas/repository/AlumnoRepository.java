package com.example.demo.notas.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.notas.model.Alumno;

@Repository
public interface AlumnoRepository extends CrudRepository<Alumno,Long>{
	Alumno findByUsername(String username);

}
