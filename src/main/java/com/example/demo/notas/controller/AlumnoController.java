package com.example.demo.notas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.notas.service.AlumnoService;
import com.example.demo.notas.service.CursoService;

@RestController
@RequestMapping("/api/v1/")
public class AlumnoController {
	
	@Autowired
	private AlumnoService service;
	
	@GetMapping("/alumnos")
	//probando 123...
	public ResponseEntity<?> encontrarTodosCursos() throws Exception {
		return  ResponseEntity.ok(service.findAll());
	}

}
