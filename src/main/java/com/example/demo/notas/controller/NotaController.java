package com.example.demo.notas.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.notas.config.JwtUtil;
import com.example.demo.notas.model.Alumno;
import com.example.demo.notas.model.Nota;
import com.example.demo.notas.service.AlumnoService;
import com.example.demo.notas.service.NotaService;

import io.jsonwebtoken.ExpiredJwtException;


@RestController
@RequestMapping("/api/v1/")
public class NotaController {
	@Autowired
	private JwtUtil jwtUtil;
	
	@Autowired
	private NotaService service;
	
	@Autowired
	private AlumnoService serviceAlumno;
	
	@GetMapping("/notas")
	public ResponseEntity<?> encontrarTodasNotas() throws Exception {
		return  ResponseEntity.ok(service.findAll());
	}
	
	@PostMapping("/notas")	
	public ResponseEntity<?> crearNota(@Valid @RequestBody Nota nota, BindingResult result) {
		if(result.hasErrors()) {
			return this.validar(result);
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(nota));
	}
	

	//para poder visualizar sus notas debe ingresar un jwt generado con el usuario del alumno
	@GetMapping("/notas/alumnos/{id}")
	public ResponseEntity<?> ObtenerNotasPorIdAlumno(HttpServletRequest request  ,@PathVariable Long id) {
		final String requestTokenHeader = request.getHeader("Authorization");
		String jwtToken = null;
		String username="";
		if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
			jwtToken = requestTokenHeader.substring(7);
			try {
				username = jwtUtil.getUsernameFromToken(jwtToken);
			} catch (IllegalArgumentException e) {
				System.out.println("inhabilitado para obtener el jwt token");
			} catch (ExpiredJwtException e) {
				System.out.println("el token jwt ha expirado");
			}
		} else {
			System.out.println("token jwt no inicio con la cadena Bearer");
		}
		Optional<Alumno> a = serviceAlumno.findAlumnoById(id);
		if(!username.equals(a.get().getUsername())) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("el jwt ingresado no pertenece al usuario alumno a consultar");
		}
		Iterable<Nota> o = service.ObtenerNotasPorIdAlumno(id);
		
		return ResponseEntity.ok().body(o);
	}
	
	protected ResponseEntity<?> validar(BindingResult result){
		Map<String,Object> errores = new HashMap<>();
		
		result.getFieldErrors().forEach(err -> {
			errores.put(err.getField(),"el campo "+ err.getField()+" "+ err.getDefaultMessage());
		});
		return ResponseEntity.badRequest().body(errores);
	}

}
