package com.example.demo.notas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendUtpApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendUtpApplication.class, args);
	}

}
